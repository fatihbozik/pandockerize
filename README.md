# Pandockerize
***

[Pandoc](https://pandoc.org) Docker Image built with Ubuntu Trusty. 
Available on [Github](https://github.com/FatihBozik/pandockerize) and [Docker Hub](https://hub.docker.com/r/bozikfatih/pandoc/).

## Basic Usage

**Pull docker image:**

    :::shell
    $ docker pull bozikfatih/pandoc



**Add an alias:**

    :::shell
    $ sudo vi ~/.bashrc
    ...
    alias pandoc='docker run -ti \
                            --name pandoc \
                            --rm -v ${PWD}:/source \
                            --rm -v /home/fatihbozik/.local/share/fonts:/usr/local/share/fonts \
                            --rm bozikfatih/pandoc'
    $ source ~/.bashrc                         



**Pandoc control:**

    :::shell
    $ pandoc -v
    pandoc 2.1.3
    Compiled with pandoc-types 1.17.4.2, texmath 0.10.1.1, skylighting 0.7.0.2
    Default user data directory: /root/.pandoc
    Copyright (C) 2006-2018 John MacFarlane
    Web:  http://pandoc.org
    This is free software; see the source for copying conditions.
    There is no warranty, not even for merchantability or fitness
    for a particular purpose.
